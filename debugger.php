<?php
/*
 *TODO:
 * Variable naam toevoegen aan debug-regel;
 *  
 */

    echo '<div class="debug-output">';
    echo '</div>';

function debug($var){
    echo '<span class="debug-message">';
    var_dump($var);
    echo '</span>';
}

function checkGlobal($global, $param){
    echo '<span class="debug-message">';
    
    // get the total number of $_GET parameters
    $numPara = count($global);

    echo "No of global parameters = $numPara <br />";

    // echo the state of the 'first' $_GET parameter
    if ($numPara > 0){
            if (isset($global[$param])) echo "1. $param parameter exists.<br />";
            if (!isset($global[$param])){ 
                echo "2. $param parameter does not exist.<br />";
            }else{
            if ($global[$param] == "") echo "3. $param parameter is empty string.<br />";
            if (empty($global[$param])) echo "4. $param parameter is empty string.<br />";
            if ($global[$param] != "") echo "5. $param parameter has non-empty string.<br />";
            if (!empty($global[$param])) echo "6. $param parameter has non-empty string.<br />";
            if ($global[$param] == null) echo "7. $param parameter is null.<br />";
            if ($global[$param] != null) echo "8. $param parameter is not null.<br />";
            }
    }
    
    echo '</span>';
}

?>
